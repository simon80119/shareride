<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index');
//會員已登入
Route::group(['middleware' => 'auth' , 'prefix' => 'user'], function () {
    Route::any('/profile', 'Auth\ProfileController@modify');
    Route::any('/reset', 'Auth\ProfileController@resetPassword');
    //共乘管理
    Route::any('/order/index', 'OrderController@index');
    Route::any('/order/create', 'OrderController@create');
    Route::any('/order/modify/{id}', 'OrderController@modify');

    //車種管理
    Route::any('/car/ajaxGetCar', 'CarController@ajaxGetCarList');
    Route::any('/car/ajaxCreateCar', 'CarController@ajaxCreateCar');

});
