<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('car_id');
            $table->integer('user_id');
            $table->integer('amount');
            $table->integer('smoke');   //是否吸菸
            $table->dateTime('datetime'); //出發時間
            $table->integer('start_place');
            $table->integer('end_place');
            $table->integer('price');
            $table->string('comment');  //備註
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
