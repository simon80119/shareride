@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                共乘列表
            </h1>
        </div>
        @if(count($orders) > 0)
            <table class="table table-striped">
                <tr>
                    <th>日期</th>
                    <th>起點</th>
                    <th>終點</th>
                    <th>共攤費</th>
                    <th>目前人數</th>
                    <th>修改</th>
                </tr>
                <tbody>
                @inject('OrderPresenter','App\Presenters\OrderPresenter')
                @foreach($orders as $order)
                    <tr>
                        <td>{{$order->datetime}}</td>
                        <td>{{ $OrderPresenter->getPlaceText()[$order->start_place] }}</td>
                        <td>{{ $OrderPresenter->getPlaceText()[$order->end_place] }}</td>
                        <td>{{$order->price}}</td>
                        <td>{{count($order->passengers)}}/{{$order->amount}}</td>
                        <td><a href="/user/order/modify/{{$order->id}}" class="btn-default">修改</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            <h3>目前沒有任何您的共乘消息，快來<a href="/user/order/create">發布訊息</a></h3>
        @endif
    </div>
</div>
@endsection
