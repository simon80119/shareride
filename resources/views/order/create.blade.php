@extends('layouts.app')
@section('head')
    <link rel="stylesheet" type="text/css" href="/js/datetimepicker-master/jquery.datetimepicker.css"/>
@endsection
@section('content')
@inject('OrderPresenter','App\Presenters\OrderPresenter')
    <div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">發布共乘消息</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ Request::url() }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('start_place') ? ' has-error' : '' }}">
                            <label for="start_place" class="col-md-4 control-label">起點</label>
                            <div class="col-md-6">
                                {{Form::select('start_place', $OrderPresenter->getPlaceText() ,isset($order->start_place)?$order->start_place:old('start_place'),['class' => 'form-control' , 'id' =>'start_place'])}}

                                @if ($errors->has('start_place'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('start_place') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('end_place') ? ' has-error' : '' }}">
                            <label for="end_place" class="col-md-4 control-label">終點</label>

                            <div class="col-md-6">
                                {{Form::select('end_place', $OrderPresenter->getPlaceText() ,isset($order->end_place)?$order->end_place:old('end_place'),['class' => 'form-control' , 'id' =>'end_place'])}}
                                @if ($errors->has('end_place'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('end_place') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('datetime') ? ' has-error' : '' }}">
                            <label for="datetime" class="col-md-4 control-label">出發日期</label>
                            <div class="col-md-6">
                                <input id="datetime" type="text" class="form-control" name="datetime" value="{{ isset($order->datetime)?$order->datetime:old('datetime') }}" onfocus="this.blur()" required>

                                @if ($errors->has('datetime'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('datetime') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('car_id') ? ' has-error' : '' }}">
                            <label for="car_id" class="col-md-4 control-label">車種</label>
                            <div class="col-md-6">
                                <select class="form-control" value="" id="car_id" name="car_id">
                                    <option value="">請選擇</option>
                                    @foreach($OrderPresenter->getCarList() as $car)
                                        <option value="{{$car->id}}" {{$OrderPresenter->getCarSelected($car->id , isset($order->car_id)?$order->car_id:old('car_id'))}}>{{$car->car_name}}({{$car->car_license}})</option>
                                    @endforeach
                                </select>
                                <button type="button" class="btn btn-primary form-control" data-toggle="modal" data-target="#myModal">
                                    新增車種
                                </button>
                                @if ($errors->has('car_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('car_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('smoke') ? ' has-error' : '' }}">
                            <label for="smoke" class="col-md-4 control-label">可否吸菸</label>
                            <div class="col-md-6">
                                {{Form::select('smoke',[ 0 => '否',1 => '是'],isset($order->smoke)?$order->smoke:old('smoke'),['class' => 'form-control' , 'id' =>'smoke'])}}
                                @if ($errors->has('smoke'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('smoke') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('amount') ? ' has-error' : '' }}">
                            <label for="amount" class="col-md-4 control-label">乘載人數</label>

                            <div class="col-md-6">
                                <input id="amount" type="number" class="form-control" name="amount" value="{{ isset($order->amount)?$order->amount:old('amount') }}" required autofocus>
                                @if ($errors->has('amount'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('amount') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                            <label for="price" class="col-md-4 control-label">共攤費</label>

                            <div class="col-md-6">
                                <input id="price" type="number" class="form-control" name="price" value="{{ isset($order->price)?$order->price:old('price') }}" required autofocus>
                                @if ($errors->has('price'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('price') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('comment') ? ' has-error' : '' }}">
                            <label for="comment" class="col-md-4 control-label">備註</label>

                            <div class="col-md-6">
                                <input id="comment" type="text" class="form-control" name="comment" value="{{ isset($order->comment)?$order->comment:old('comment') }}" autofocus>
                                @if ($errors->has('comment'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('comment') }}</strong>
                                    </span>
                                @endif
                                <span class="help-block">
                                    EX:不可帶寵物 / 沿途可停
                                </span>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    送出
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">新增車種</h4>
            </div>
            <div class="modal-body">
                <form id="create-car-form" class="form-horizontal" role="form" method="POST" action="/user/car/ajaxCreateCar">
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('car_name') ? ' has-error' : '' }}">
                        <label for="car_name" class="col-md-4 control-label">車種名稱</label>
                        <div class="col-md-6">
                            <input id="car_name" type="text" class="form-control" name="car_name" value="{{ old('car_name') }}" required autofocus>
                            <span class="help-block">
                                EX:Toyata Yaris
                            </span>
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('car_license') ? ' has-error' : '' }}">
                        <label for="car_license" class="col-md-4 control-label">車牌後四碼</label>

                        <div class="col-md-6">
                            <input id="car_license" type="text" class="form-control" name="car_license" value="{{ old('car_license') }}" required autofocus>
                            <span class="help-block">
                                EX:車牌AAA-1234，請填入1234
                            </span>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">關閉</button>
                <button id="create-car-submit" type="button" class="btn btn-primary">送出</button>
            </div>
        </div>
    </div>
</div>
@endsection
@section('footer')

    {{--時間選擇器載入--}}
    <script src="/js/datetimepicker-master/jquery.js"></script>
    <script src="/js/datetimepicker-master/build/jquery.datetimepicker.full.min.js"></script>


    <script src="/js/order/create.js"></script>
@endsection
