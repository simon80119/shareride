@extends('layouts.app')

@section('content')
@inject('OrderPresenter','App\Presenters\OrderPresenter')
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                找不到人共乘嗎？快發布共乘消息吧！
            </h1>
        </div>
        @foreach($orders as $order)
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>{{ $OrderPresenter->getPlaceText()[$order->start_place] }}->{{ $OrderPresenter->getPlaceText()[$order->end_place] }}</h4>
                </div>
                <div class="panel-body">
                    <p>時間：{{$order->datetime}}</p>
                    <p>人數：{{$order->amount}}</p>
                    <a href="#" class="btn btn-default">立即共乘</a>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
@endsection
