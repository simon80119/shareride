<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class ProfileController extends Controller
{
    public function modify(Request $request)
    {
        $user = Auth::user();
        if ($request->isMethod('post')) {
            $rule = [
                'name' => 'required|max:255',
                'mobile_number' => 'required',
                'sex' => 'required',
            ];
            $message = [
                'name.required' => '請輸入姓名',
                'mobile_number.required' => '請輸入手機號碼',
            ];
            $validator = \Validator::make($request->all(), $rule , $message);
            if ($validator->fails()) {
                return redirect('user/profile')
                    ->withErrors($validator)
                    ->withInput();
            }
            $user->mobile_number = Input::get('mobile_number');
            $user->name = Input::get('name');
            $user->sex = Input::get('sex');
            $user->save();
            return redirect('/');
        }
        return view('auth.profile')->with('user',$user);
    }

    public function resetPassword(Request $request)
    {
        if ($request->isMethod('post')) {
            $rule = [
                'password' => 'required|min:6|confirmed'
            ];
            $message = [
                'password.required' => '請輸入密碼',
                'password.confirmed' => '與確認密碼不相符'
            ];
            $validator = \Validator::make($request->all(), $rule , $message);
            if ($validator->fails()) {
                return redirect('user/reset')
                    ->withErrors($validator)
                    ->withInput();
            }
            $user = Auth::user();
            $user->password = bcrypt(Input::get('password'));
            $user->save();
            return redirect('/');
        }
        return view('auth.passwords.login_reset');
    }
}
