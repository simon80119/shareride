<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;

class OrderController extends Controller
{
    //列表
    public function index()
    {
        $orders = Order::User(\Auth::user()->id)->paginate(15);

        return view('order.home')->with('orders',$orders);
    }
    //新增消息
    public function create(Request $request)
    {

        if($request->isMethod('post')){
            $rule = [
                'start_place'   => 'required|integer',
                'end_place'     => 'required|integer',
                'datetime'      => 'required',
                'car_id'        => 'required|integer',
                'smoke'         => 'required|integer',
                'amount'        => 'required|integer',
                'price'         => 'required|integer',
            ];
            $validator = \Validator::make($request->all(), $rule);
            if ($validator->fails()) {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }
            $order = new order();
            $order->start_place = $request->start_place;
            $order->end_place   = $request->end_place;
            $order->datetime    = $request->datetime;
            $order->car_id      = $request->car_id;
            $order->user_id      = \Auth::user()->id;
            $order->smoke       = $request->smoke;
            $order->amount      = $request->amount;
            $order->price       = $request->price;
            $order->comment     = $request->comment;
            $order->save();
            return redirect('/user/order/index');
        }
        return view('order.create');
    }

    public function modify(Request $request ,$id)
    {
        $order = Order::find($id);
        if($request->isMethod('post')){
            $rule = [
                'start_place'   => 'required|integer',
                'end_place'     => 'required|integer',
                'datetime'      => 'required',
                'car_id'        => 'required|integer',
                'smoke'         => 'required|integer',
                'amount'        => 'required|integer',
                'price'         => 'required|integer',
            ];
            $validator = \Validator::make($request->all(), $rule);
            if ($validator->fails()) {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }
            $order->start_place = $request->start_place;
            $order->end_place   = $request->end_place;
            $order->datetime    = $request->datetime;
            $order->car_id      = $request->car_id;
            $order->user_id      = \Auth::user()->id;
            $order->smoke       = $request->smoke;
            $order->amount      = $request->amount;
            $order->price       = $request->price;
            $order->comment     = $request->comment;
            $order->save();
            return redirect('/user/order/index');
        }
        return view('order.create')->with('order',$order);
    }
}
