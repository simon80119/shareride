<?php

namespace App\Http\Controllers;

use App\Car;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class CarController extends Controller
{
    //
    public function ajaxGetCarList()
    {
        $cars = Car::User(\Auth::user()->id)->select('id','car_name','car_license')->get()->toArray();
        if(isset($cars) && !empty($cars)){
            return response()->json(['result' => 'success' , 'cars' => $cars]);
        }
        return response()->json(['result' => 'failed']);
    }
    public function ajaxCreateCar(Request $request)
    {
        $rule = [
            'car_name'   => 'required',
            'car_license'   => 'required',
        ];
        $validator = \Validator::make($request->all(), $rule);
        if ($validator->fails()) {
            return response()->json(['result' => 'failed']);
        }
        $car = new Car();
        $car->user_id = \Auth::user()->id;
        $car->car_name = Input::get('car_name');
        $car->car_license = Input::get('car_license');
        $car->save();
        return response()->json(['result' => 'success']);
    }
}
