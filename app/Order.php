<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    //
    const STATUS_DISABLE = 0;
    const STATUS_ENABLE = 1;

    public function passengers()
    {
        return $this->hasMany('App\Passenger');
    }
    public function scopeUser($query ,$user_id)
    {
        return $query->where('user_id' , $user_id);
    }
}
