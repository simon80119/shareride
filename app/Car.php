<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    //
    public function scopeUser($query ,$user_id)
    {
        return $query->where('user_id' , $user_id);
    }
}
