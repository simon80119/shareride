<?php
/**
 * Created by PhpStorm.
 * User: wangyucheng
 * Date: 2016/12/4
 * Time: 下午4:21
 */

namespace App\Presenters;


use App\Car;

class OrderPresenter
{
    static $place_text = [
        '1' => '基隆市',
        '2' => '台北市',
        '3' => '新北市',
        '4' => '桃園市',
        '5' => '新竹市',
        '6' => '新竹線',
        '7' => '苗栗縣',
        '8' => '台中市',
        '9' => '南投縣',
        '10' => '嘉義縣',
        '11' => '雲林縣',
        '12' => '嘉義市',
        '13' => '嘉義縣',
        '14' => '台南市',
        '15' => '高雄市',
        '16' => '屏東縣',
        '17' => '台東縣',
        '18' => '花蓮縣',
        '19' => '宜蘭縣',
    ];

    public function getPlaceText()
    {
        return self::$place_text;
    }

    public function getCarList()
    {
        $cars = Car::User(\Auth::user()->id)->select('id','car_name','car_license')->get();

        return $cars;
    }

    public function getCarSelected($car_id ,$order_car_id)
    {
        if($car_id == $order_car_id){
            return 'selected';
        }
        return "";
    }
}