/**
 * Created by wangyucheng on 2016/12/3.
 */
$('#datetime').datetimepicker();
$(function() {
    $("#create-car-form").submit(function(e)
    {
        var postData = $(this).serializeArray();
        var formURL = $(this).attr("action");
        $.ajax(
            {
                url : formURL,
                type: "POST",
                data : postData,
                success:function( response )
                {
                    if(response.result == 'failed'){
                        alert('請將訊息填寫完整');
                    }else if( response.result == 'success' ){
                        $('#myModal').modal('hide');
                        $("#create-car-form")[0].reset();
                        ajaxGetCar();
                    }
                }
            });
        e.preventDefault(); //STOP default action
    });
    $("#create-car-submit").click(function () {
        $("#create-car-form").submit();
    });
});
function ajaxGetCar() {
    $.ajax({
        url: "/user/car/ajaxGetCar",
        dataType: "json",
        beforeSend: function(){
            $("#car_id option").remove();
        },
        success: function( response ){
        if( response.result == 'failed' ){
            $("#car_id").append($("<option></option>").attr("value", "").text("目前沒有車種"));
        }else if ( response.result == 'success' ){
            $("#car_id").append($("<option></option>").attr("value", "").text("請選擇"));
            $.each(response.cars, function( index, value ) {
                $("#car_id").append($("<option></option>").attr("value", value.id).text(value.car_name + '(' + value.car_license + ')'));
            });
        }
    }});
}